package testClases;

import org.testng.annotations.Test;
import page.Base;
import utils.BaseConfig;

public class CPA001_Poliza extends BaseConfig {

    @Test(description = "Valida Login y Poliza",groups = "login")
    public void loginPoliza(){
        Base base = new Base();
        base.clickPoliza();
    }
}
