package utils;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import driver.DriverContext;
import org.openqa.selenium.WebDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.lang.reflect.Method;

import static constans.Constants.urlNegocioImed;
import static constans.Navegador.Chrome;

public class BaseConfig {

    public WebDriver driver;
    ExtentReports report;
    ExtentHtmlReporter htmlReporter;
    public String nameClass = "";
    public static ExtentTest test;
    public static HtmlReport htrep;

    @BeforeMethod(alwaysRun = true)
    public void beforeMethodSetup(Method method) throws Exception {
        DriverContext.setUp(Chrome, urlNegocioImed);
        DriverContext.setDriverTimeout(5);
        DriverContext.setScriptTimeout(5);
        htrep = new HtmlReport(driver);
        report = htrep.getReport();
        nameClass = this.getClass().getName().substring(this.getClass().getPackage().getName().length() + 1,
                this.getClass().getName().length());
        String description = "";
        Test testName = method.getAnnotation(Test.class);
        description = (testName != null) ? testName.description() : "";
        test = report.createTest(nameClass, description);
    }

    @AfterMethod(alwaysRun = true)
    public void afterMethodSetup(ITestResult result) {
        DriverContext.deleteAllCookies();
        System.out.println("State Case = " + htrep.returnStateCase(result));
        htrep.generateStep(test, result, "Caso ejecutado " + nameClass);
        report.flush();
        htrep.leerHtml();
        DriverContext.quitDriver();
    }
}
