package page;

import driver.DriverContext;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utils.MetodosGenericos;

public class Base {

    private WebDriver driver;
    public Base() {
        this.driver = DriverContext.getDriver();
        PageFactory.initElements(this.driver, this);
    }

    @FindBy(xpath = "//*[@id=\"navlink-polici\"]")
    private WebElement linkPoliza;

    public void clickPoliza(){

        boolean poliza = MetodosGenericos.visualizarObjeto(linkPoliza, 5);

        if (poliza) {
            linkPoliza.click();
            System.out.println("se encuentra visible el  botón editar contacto");
        } else {
            System.out.println("No se despliega el boton editar contacto");
        }
    }
}
